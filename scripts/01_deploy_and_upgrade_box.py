import scripts.helpers
from brownie import network, Box, BoxV2, ProxyAdmin, TransparentUpgradeableProxy, Contract, config, network

def main():
    deploy_account = scripts.helpers.get_account(0)
    print(f"Deploying to {network.show_active()}")
    box = Box.deploy({"from":deploy_account}, 
                     publish_source=config["networks"][network.show_active()]["verify"])
    print(f"Deployed {network.show_active()}")
    print(box.retrieve())
    
    proxyAdmin = ProxyAdmin.deploy({"from":deploy_account}, 
                                   publish_source=config["networks"][network.show_active()]["verify"])
    #initializer = box.store, 1
    box_encoded_initializer_function = scripts.helpers.encode_function_data()
    
    proxy = TransparentUpgradeableProxy.deploy(box.address, proxyAdmin.address, 
                                    box_encoded_initializer_function,
                                    {"from":deploy_account, "gas_limit":1000000},
                                    publish_source=config["networks"][network.show_active()]["verify"])
                                    
    print(f"proxy deployed to {proxy}, you can now upgrade to V2")

    proxy_box = Contract.from_abi("Box", proxy.address, Box.abi)

    tx = proxy_box.store(2,{"from":deploy_account})
    tx.wait(1)
    print(proxy_box.retrieve())

    box_v2 = BoxV2.deploy({"from":deploy_account},
                          publish_source=config["networks"][network.show_active()]["verify"])
    upgrade_tx = scripts.helpers.upgrade_implementation_contract(deploy_account, proxy, box_v2.address, proxyAdmin)
    upgrade_tx.wait(1)
    
    proxy_box = Contract.from_abi("BoxV2", proxy.address, BoxV2.abi)
    print(proxy_box.retrieve())
    proxy_box.increment({"from":deploy_account})
    print(proxy_box.retrieve())