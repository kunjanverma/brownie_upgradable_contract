import eth_utils
from brownie import network, accounts, config

LOCAL_BLOCKCHAIN_ENVIRONMENTS = ["development", "easy-trail"]
FORKED_LOCAL_BLOCKCHAINS = ["mainnet-fork"]

def get_account(index):
    if network.show_active() in LOCAL_BLOCKCHAIN_ENVIRONMENTS or \
       network.show_active() in FORKED_LOCAL_BLOCKCHAINS:
        return accounts[index]
    else:
        return accounts.add(config["wallets"]["private_key_" + str(index)])

def encode_function_data(initializer=None, *args):
    if len(args) == 0 or not initializer:
        return eth_utils.to_bytes(hexstr='0x')
    return initializer.encode_input(*args)

def upgrade_implementation_contract(account, proxy, newImplementationAddress, proxyAdminContract=None,
                                    initializer = None, *args):
    transaction = None
    if proxyAdminContract:
        if initializer:
            encoded_function_call = encode_function_data(initializer, *args)
            transaction = proxyAdminContract.upgradeAndCall(proxy.address, 
                                                            newImplementationAddress,
                                                            encoded_function_call,
                                                            {"from":account})
        else:
            transaction = proxyAdminContract.upgrade(proxy.address, 
                                                     newImplementationAddress,
                                                     {"from":account})
    else: 
        if initializer:
            encoded_function_call = encode_function_data(initializer, *args)
            transaction = proxy.upgradeToAndCall(newImplementationAddress,
                                                encoded_function_call,
                                                {"from":account})
        else:
            transaction = proxy.upgradeToAndCall(newImplementationAddress, {"from":account})
        
    return transaction    