import scripts.helpers
from brownie import Box, ProxyAdmin, TransparentUpgradeableProxy, Contract

def test_proxy_delegates_calls():
    deploy_account = scripts.helpers.get_account(0)
    box = Box.deploy({"from":deploy_account})
    proxyAdmin = ProxyAdmin.deploy({"from":deploy_account})
    box_encoded_initializer_function = scripts.helpers.encode_function_data()
    transparentUpgradeableProxy = TransparentUpgradeableProxy.deploy(box.address,
                                                              proxyAdmin.address,
                                                              box_encoded_initializer_function,
                                                              {"from":deploy_account, "gas_limit":1000000})
    proxy_box = Contract.from_abi("Box", transparentUpgradeableProxy.address, box.abi)
    assert proxy_box.retrieve() == 0
    tx = proxy_box.store(2,{"from":deploy_account})
    tx.wait(1)
    assert proxy_box.retrieve() == 2
    