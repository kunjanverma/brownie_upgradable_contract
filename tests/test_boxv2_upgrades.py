import scripts.helpers, pytest
from brownie import Box, BoxV2, ProxyAdmin, TransparentUpgradeableProxy, Contract, exceptions

def test_proxy_upgrade():
    deploy_account = scripts.helpers.get_account(0)
    box = Box.deploy({"from":deploy_account})
    proxyAdmin = ProxyAdmin.deploy({"from":deploy_account})
    box_encoded_initializer_function = scripts.helpers.encode_function_data()
    transparentUpgradeableProxy = TransparentUpgradeableProxy.deploy(box.address,
                                                              proxyAdmin.address,
                                                              box_encoded_initializer_function,
                                                              {"from":deploy_account, "gas_limit":1000000})
    box_v2 = BoxV2.deploy({"from":deploy_account})
    proxy_box = Contract.from_abi("BoxV2", transparentUpgradeableProxy.address, box_v2.abi)
    
    with pytest.raises(exceptions.VirtualMachineError):
        proxy_box.increment({"from":deploy_account})
   
    # Now upgrade the proxy with BoxV2 contract
    tx = scripts.helpers.upgrade_implementation_contract(deploy_account, transparentUpgradeableProxy,
                                                        box_v2.address, proxyAdmin)
    tx.wait(1)
    assert proxy_box.retrieve() == 0 
    proxy_box.increment({"from":deploy_account})
    assert proxy_box.retrieve() == 1 
                                                    